# configs_tweaker

This script let's you change parts of a text file based on what's in it.

The idea behind the proyect was to have something that would let you change *just* that pice of the default config you didn't like, and keep the rest as it is. Also, it proved itself useful to change, for example, the verbose level of a config file in a dev environment, in case shell variables are not available.

With that in mind, this program will do the following:

- Append any text to the end or beggining of a file
- Insert any text before or after the first appearence of any specified 
- Change the first appearence of any specified text for any other text
- Do the things above, specifying if you want to add a newline before, after, or none at all
