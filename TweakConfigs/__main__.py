from ConfigsTweaker.main import change
from ConfigsTweaker.args_parser import parse_arguments

args = parse_arguments()
try:
    change(
        file=args.file,
        input_data=args.input,
        mode=args.mode,
        trigger=args.trigger,
        newline=args.newline,
        no_path=args.no_path,
    )
except FileNotFoundError:
    print("{} was not found".format(args.input))
except:
    print("There was a problem running the program.")
