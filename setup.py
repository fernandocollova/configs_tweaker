"""
Copyright (c) 2017 Fernando Collova

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""

from setuptools import setup

setup(
    name='ConfigsTweaker',
    packages=['ConfigsTweaker', "TweakConfigs"],
    version="asdf",
    description='ConfigsTweaker',
    scripts=['tweak_configs'],
    data_files=[]
)

# Install with:
# python3 -m pip install --upgrade (--no-cache-dir)
# git+ssh://git@bitbucket.org/fernandocollova/configs_tweaker.git#egg=ConfigsTweaker
