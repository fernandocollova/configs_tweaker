"""
Copyright (c) 2017 Fernando Collova

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""

from .commons import find_index

def insert_after(original_data, trigger, new_data):
    index = find_index(original_data, trigger)
    return original_data[:(index + len(trigger))] \
        + new_data \
        + original_data[(index + len(trigger)):]

def insert_before(original_data, trigger, new_data):
    index = find_index(original_data, trigger)
    return new_data + original_data[:index] +  original_data[index:]

def change_text(original_data, trigger, new_data):
    index = find_index(original_data, trigger)
    return original_data[:index] + new_data + original_data[(index + len(trigger)):]

def change_line(original_data, trigger, new_data):
    index = find_index(original_data, trigger)
    try:
        newline_index = find_index(original_data[index:], "\n") + index
    except ValueError as e: # Assume it's the last line of the file
        del e
        return original_data[:index] + new_data
    return original_data[:index] + new_data + original_data[newline_index:]

def add_line(original_data, new_data):
    return original_data + "\n" + new_data
