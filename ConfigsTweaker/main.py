from ConfigsTweaker.modifiers import (
    insert_after, insert_before, change_text, change_line, add_line
)

from pathlib import Path

def change(file=None, input_data=None, mode=None, trigger=None, newline=None, no_path=False):

    with open(file, mode="r", encoding='utf-8') as original_file:
        original_data = original_file.read()

    if no_path:
        new_data = input_data
    else:
        path = Path(input_data)
        with open(path, mode="r", encoding='utf-8') as input_data_file:
            new_data = input_data_file.read()[:-1]

    if newline:
        if newline == 'after':
            new_data = new_data + "\n"
        elif newline == 'before':
            new_data = "\n" + new_data
        elif newline == 'both':
            new_data = "\n" + new_data + "\n"

    modes_with_trigger = [
        'insert_after',
        'insert_before',
        'change',
        'change_line',
    ]

    if mode in modes_with_trigger:
        try:
            with open(trigger, mode="r", encoding='utf-8') as input_data_file:
                trigger = input_data_file.read()[:-1]
        except FileNotFoundError as e:
            del e
            trigger = trigger
        except TypeError:
            if getattr(input_data, "__str__") and getattr(mode, "__str__"):
                raise TypeError(
                    "{} is not a valid trigger for the {} mode"
                    .format(str(trigger), str(mode))
                )
            else:
                raise TypeError("trigger is not valid")

    if mode == 'insert_after':
        full_data = insert_after(original_data, trigger, new_data)
    elif mode == 'insert_before':
        full_data = insert_before(original_data, trigger, new_data)
    elif mode == 'change':
        full_data = change_text(original_data, trigger, new_data)
    elif mode == 'change_line':
        full_data = change_line(original_data, trigger, new_data)
    elif mode == 'add_line':
        full_data = add_line(original_data, new_data)
    else:
        if getattr(input_data, "__str__"):
            raise TypeError("{} is not a valid mode".format(str(mode)))
        else:
            raise TypeError("A valid mode was not provided")

    with open(file, mode="w", encoding='utf-8') as file:
        file.write(full_data)
