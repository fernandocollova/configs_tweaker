"""
Copyright (c) 2017 Fernando Collova

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""

import argparse


def parse_arguments():

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-f', '--file',
        type=str,
        help="Config file to midify",
        dest='file',
        required=True,
    )

    parser.add_argument(
        '-i', '--input',
        type=str,
        help="Data input",
        dest='input',
        required=True,
    )

    parser.add_argument(
        '-m', '--mode',
        choices=['insert_after', 'insert_before', 'change', 'change_line'],
        default='insert_before',
        help="Operation mode",
        dest='mode',
        required=True,
    )

    parser.add_argument(
        '-t', '--trigger',
        type=str,
        help="Trigger for the action",
        dest='trigger',
    )

    parser.add_argument(
        '-n', '--new_line',
        choices=['after', 'before', 'both'],
        help="Options for where to add new lines",
        dest='newline',
    )
    parser.add_argument(
        '--no-path',
        help='Treat "-i" "--input" as raw data',
        dest='no_path',
        action='store_true',
        default=False,
    )
    return parser.parse_args()
